section .data
    mensaje db 'Ingrese un numero: ',10
    lmensaje equ $-mensaje
    msg_presentacion db 10,'El numero ingresado es = '
    lmsg_presentacion equ $-msg_presentacion
section .bss
    numero resb  5
section .text
    global _start

_start:
    mov eax, 4
    mov ebx, 1
    mov ecx, mensaje
    mov edx, lmensaje
    int 80h 

    mov al,0
    add al,'0'
    mov [numero],al

    mov eax, 4
    mov ebx, 1
    mov ecx, msg_presentacion
    mov edx, lmsg_presentacion
    int 80h

    mov eax, 4
    mov ebx, 1
    mov ecx, numero
    mov edx, 5
    int 80h
;Salir
    mov eax, 1
	mov ebx, 0
	int 80h  