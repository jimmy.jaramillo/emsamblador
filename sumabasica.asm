section .data
    mensaje db 'Ingrese un numero: ',10
    lmensaje equ $-mensaje
    salto db 10
    lsalto equ $-salto
    msg_presentacion db 10,'La Suma es = '
    lmsg_presentacion equ $-msg_presentacion
section .bss
    numero1 resb 2
    numero2 resb 2
    suma resb 2
section .text
    global _start

_start:
;PEDIR PRIMER NUMERO
    mov eax, 4
    mov ebx, 1
    mov ecx, mensaje
    mov edx, lmensaje
    int 80h 
;LEER PRIMER NUMERO
    mov eax, 3
    mov ebx, 0
    mov ecx, numero1   
    mov edx, 2
    int 80h
;PEDIR SEGUNDO NUMERO
    mov eax, 4
    mov ebx, 1
    mov ecx, mensaje
    mov edx, lmensaje
    int 80h 
;LEER SEGUNDO NUMERO
    mov eax, 3
    mov ebx, 0
    mov ecx, numero2   
    mov edx, 2
    int 80h  
;SUMAR
    ; Movemos los numeros ingresados a los registro 
    mov al, [numero1]
	mov bl, [numero2]
    ; Convertimos los valores ingresados de ascii a decimal
    sub al, '0'
	sub bl, '0'
    ;Numero1 + Numero2
    add al, bl
    ;Convertir el resultado a decimal
    add al, '0'
    
    mov [suma], al

;Presenta el mensaje de resultado
    mov eax, 4
    mov ebx, 1
    mov ecx, msg_presentacion
    mov edx, lmsg_presentacion
    int 80h
;Presenta el resultado
    mov eax, 4
    mov ebx, 1
    mov ecx, suma
    mov edx, 2
    int 80h
;Da un salto
    mov eax, 4
    mov ebx, 1
    mov ecx, salto
    mov edx, lsalto
    int 80h
    
;Salir
    mov eax, 1
	mov ebx, 0
	int 80h  