;Jimmy Jaramillo
;22 de Julio,2020
section .data
        asterisco db '*'
        salto db 10

section .text
        global _start
_start:
        mov rax,9

saltar: 
        mov rcx,9
        dec rax
        push rax
        jmp imprimir        

imprimir:
    ;Imprime una linea de Asteriscos
        dec rcx
        push rcx
        mov rax,4
        mov ebx,1
        mov rcx,asterisco
        mov edx,1
        int 80h
        pop rcx
        cmp rcx,0
        jnz imprimir
    ;Da un enter
        mov rax,4
        mov ebx,1
        mov rcx,salto
        mov edx,1
        int 80h
        ;Compara los verticales
        pop rax
        cmp rax,0
        jnz saltar
        
salir:
        mov rax,1
        int 80h