; Jimmy Jaramillo
; 22/06/2020

%macro imprimir 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro

%macro leerNumero 1
    mov eax, 3
    mov ebx, 0
    mov ecx, %1
    mov edx, 2
    int 80h
%endmacro


section .data
    msg     db  10,10,"El Resultado de la Suma es: "
    lmsg:  equ $-msg
    msg1     db  10,"El Resultado de la Resta es: "
    lmsg1:  equ $-msg1
    msg2     db  10,"El Resultado de la Multiplicacion  es: "
    lmsg2:  equ $-msg2
    msg3     db  10,"El Resultado de la Division es: "
    lmsg3:  equ $-msg3
    msg4     db  10,"Ingrese el Primer Numero: "
    lmsg4:  equ $-msg4
    msg5    db  10,"Ingrese el Segundo Numero: "
    lmsg5:  equ $-msg5
    msg6     db  10,"El Residuo de la Division es: "
    lmsg6:  equ $-msg6

section .bss
    product resb 2
    res resb 2
    num1 resb 2
    num2 resb 2

section .text
    global _start

_start:
    ; LEER
    imprimir msg4,lmsg4
    leerNumero num1
    imprimir msg5,lmsg5
    leerNumero num2
    ; SUMAR
    ; Movemos los numeros ingresados a los registro
    mov al, [num1]
    mov bl, [num2]

    ; Convertimos los valores ingresados de ascii a decimal
    sub al, '0'
    sub bl, '0'

    ; Numero1 + Numero2
    add al, bl
    ; Convertir el resultado a decimal
    add al, '0'

    mov [product], al
    imprimir msg,lmsg
    imprimir product,1

    ; RESTA
    ; Movemos los numeros a los registro
    mov al, [num1]
    mov bl, [num2]
    ; Convertimos los valores ingresados de ascii a decimal
    sub al, '0'
    sub bl, '0'
    ; Restamos el registro AL y BL
    sub al, bl

    ; Convertimos el resultado de la resta de decimal a ascii
    add al, '0'

    ; Movemos el resultado a un espacio reservado en la memoria
    mov [product], al

    ; Imprimimos el mensaje
    imprimir msg1,lmsg1
    imprimir product,1
    ; MULT
    ; Movemos los numeros a los registro
    mov al, [num1]
    mov bl, [num2]
    ; Convertimos los valores ingresados de ascii a decimal
    sub al, '0'
    sub bl, '0'

    ; Multiplicamos
    mul bl

    ; Convertimos el resultado
    add ax, '0'

    ; Movemos el resultado
    mov [product], ax

    ; Imprimimos el mensaje
    imprimir msg2,lmsg2
    imprimir product,1
    ; DIVISION
    ; Movemos los numeros ingresados a los registro AL y BL
    mov al, [num1]
    mov bl, [num2]
    ; Convertimos los valores ingresados de ascii a decimal
    sub al, '0'
    sub bl, '0'

    ; Igualamos a cero los registros DX y AH
    mov dx, 0
    mov ah, 0

    ; Division.
    div bl

    ; Convertimos el resultado de la resta de decimal a ascii
    add ax, '0'
    add ah,'0'

    ; Movemos el resultado a un espacio reservado en la memoria
    mov [product], ax
    mov [res], ah

    ; Imprimimos el mensaje
    imprimir msg3,lmsg3
    imprimir product,1
    imprimir msg6,lmsg6
    imprimir res,1
    
    ; Salir
    mov eax, 1
    mov ebx, 0
    int 80h