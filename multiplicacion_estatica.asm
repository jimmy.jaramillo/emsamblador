;nasm 2.13.02
;Nombre : Jimmy Jaramillo
section .data   
    msg     db  10,"El Resultado es: "   
    lmsg:  equ $-msg
section .bss
    product resb 2
section .text
	global _start

_start:
	; Movemos los numeros a los registro 
	mov al, 2
	mov bl, 4
 
	; Multiplicamos. AX = AL X BL
	mul bl
 
	; Convertimos el resultado
	add ax, '0'
 
	; Movemos el resultado
	mov [product], ax
 
	; Imprimimos el mensaje 
	mov eax, 4
	mov ebx, 1
	mov ecx, msg
	mov edx, lmsg
	int 80h
 
	; Imprimimos  el resultado
	mov eax, 4
	mov ebx, 1
	mov ecx, product
	mov edx, 2
	int 80h
 
	; Salir
    mov eax, 1
	mov ebx, 0
	int 80h 