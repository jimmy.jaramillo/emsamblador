if [ $# -eq 1 ] 
then
    nasm -f elf64 -o $1.o $1.asm
    ld -o $1 $1.o
    ./$1
else 
    echo "#########"
    echo "Error 404"
fi

# nasm -f elf64 example.asm # assemble the program  
# ld -s -o example example.o # link the object file nasm produced into an executable file  
# ./example # example is an executable file