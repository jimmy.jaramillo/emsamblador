;nasm 2.13.02
;Nombre : Jimmy Jaramillo
section .data  
    msg     db  10,"El Resultado es: "   
    lmsg:  equ $-msg
section .bss
    product resb 2
section .text
	global _start

_start:
; Movemos los numeros ingresados a los registro AL y BL
	mov al, 8
	mov bl, 2    
 
	; Igualamos a cero los registros DX y AH
	mov dx, 0
	mov ah, 0
 
	; Division. AL = AX / BL. AX = AH:AL
	div bl
 
	; Convertimos el resultado de la resta de decimal a ascii
	add ax, '0'
 
	; Movemos el resultado a un espacio reservado en la memoria
	mov [product], ax
 
	; Imprimimos el mensaje 
	mov eax, 4
	mov ebx, 1
	mov ecx, msg
	mov edx, lmsg
	int 80h
 
	; Imprimimos  el resultado
	mov eax, 4
	mov ebx, 1
	mov ecx, product
	mov edx, 2
	int 80h
 
	; Salir
    mov eax, 1
	mov ebx, 0
	int 80h 