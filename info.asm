section .data
    nombre db "Jimmy "
    lnombre equ $ - nombre

    apellido db "Jaramillo",10
    lapellido equ $ - apellido

    materia db "Lenguaje Emsamblador",10
    lmateria equ $ - materia

    genero db "Masculino",10
    lgenero equ $ - genero

;section .bss

section .text
    global _start
_start:
    ;IMPRIMIR nombre
    mov eax, 4
    mov ebx, 1
    mov ecx, nombre
    mov edx, lnombre
    int 80h 

     ;IMPRIMIR apellido
    mov eax, 4
    mov ebx, 1
    mov ecx, apellido
    mov edx, lapellido
    int 80h 
     ;IMPRIMIR materia
    mov eax, 4
    mov ebx, 1
    mov ecx, materia
    mov edx, lmateria
    int 80h 

     ;IMPRIMIR genero
    mov eax, 4
    mov ebx, 1
    mov ecx, genero
    mov edx, lgenero
    int 80h 
    ;FINALIZAR PROGRAMA
    mov eax, 1
	mov ebx, 0
	int 80h