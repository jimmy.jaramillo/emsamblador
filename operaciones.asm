%macro imprimir 2
	mov eax, 4
	mov ebx, 1
	mov ecx, %1
	mov edx, %2
	int 80h
%endmacro

section .data   
    msg     db  10,"El Resultado de la Suma es: "   
    lmsg:  equ $-msg
    msg1     db  10,"El Resultado de la Resta es: "   
    lmsg1:  equ $-msg1
    msg2     db  10,"El Resultado de la Multiplicacion  es: "   
    lmsg2:  equ $-msg2
    msg3     db  10,"El Resultado de la Division es: "   
    lmsg3:  equ $-msg3
section .bss
    product resb 2
section .text
	global _start
_start:
;SUMAR
    ; Movemos los numeros ingresados a los registro 
    mov al, 4
	mov bl, 2
    ;Numero1 + Numero2
    add al, bl
    ;Convertir el resultado a decimal
    add al, '0'
    
    mov [product], al
    imprimir msg,lmsg
    imprimir product,1

;RESTA
;Movemos los numeros a los registro 
	mov al, 4
	mov bl, 2
	; Restamos el registro AL y BL
	sub al, bl
 
	; Convertimos el resultado de la resta de decimal a ascii
	add al, '0'
 
	; Movemos el resultado a un espacio reservado en la memoria
	mov [product], al
 
	; Imprimimos el mensaje 
    imprimir msg1,lmsg1
    imprimir product,1
;MULT
; Movemos los numeros a los registro 
	mov al, 2
	mov bl, 4
 
	; Multiplicamos. AX = AL X BL
	mul bl
 
	; Convertimos el resultado
	add ax, '0'
 
	; Movemos el resultado
	mov [product], ax
 
	; Imprimimos el mensaje 
	imprimir msg2,lmsg2
    imprimir product,1
;DIVISION
; Movemos los numeros ingresados a los registro AL y BL
	mov al, 4
	mov bl, 2    
 
	; Igualamos a cero los registros DX y AH
	mov dx, 0
	mov ah, 0
 
	; Division. AL = AX / BL. AX = AH:AL
	div bl
 
	; Convertimos el resultado de la resta de decimal a ascii
	add ax, '0'
 
	; Movemos el resultado a un espacio reservado en la memoria
	mov [product], ax
 
	; Imprimimos el mensaje 
    imprimir msg3,lmsg3
    imprimir product,1
 
	; Salir
    mov eax, 1
	mov ebx, 0
	int 80h 