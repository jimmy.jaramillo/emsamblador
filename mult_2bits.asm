;nasm 2.13.02
;Nombre : Jimmy Jaramillo
section .data   
    msg     db  10,"El Resultado es: "   
    lmsg:  equ $-msg
section .bss
    product resb 2
section .text
	global _start

_start:
	; Movemos los numeros a los registro 
	mov al, 9
	mov bl, 9
 
	; Multiplicamos. AX = AL X BL
	mul bl
    aam
 
	; Convertimos el resultado
	add ah, '0'
	add al, '0'
 
	; Movemos el resultado
	mov [product+0], ah
    mov [product+1], al
 
	; Imprimimos el mensaje 
	mov eax, 4
	mov ebx, 1
	mov ecx, msg
	mov edx, lmsg
	int 80h
 
	; Imprimimos  el resultado
	mov eax, 4
	mov ebx, 1
	mov ecx, product
	mov edx, 2
	int 80h
 
	; Salir
    mov eax, 1
	mov ebx, 0
	int 80h 